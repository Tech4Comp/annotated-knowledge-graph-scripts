var http = require('http'); // Import Node.js core module
var materials = require('./get_additional_material_for');
var compareTerms = require('./compare_central_terms_to');
var materialKeys = require('./list_nodes_with_additional_material');
const fuzz = require('fuzzball');
const _ = require('lodash');

const acceptance = 85;
if(process.env.sparqlEndpoint == null){
    process.env.sparqlEndpoint = 'https://triplestore.tech4comp.dbis.rwth-aachen.de/Wissenslandkarten';
} 
const sparqlEndpoint = process.env.sparqlEndpoint;
var graph = 'http://tech4comp.de/knowledgeMap/ul/biwi/laura'

var server = http.createServer(function (req, res) {   //create web server
    if (req.url == '/') { //check the URL of the current request
        console.log("my value is" + process.env.test);
        // set response header
        res.writeHead(200, { 'Content-Type': 'text/html' }); 
        
        // set response content    
        res.write('ok');
        res.end();
    
    }
    else if (req.url == "/materials" && req.method === 'POST') {
        let body = '';
        req.on('data', chunk => {
            body += chunk.toString(); // convert Buffer to string
        });
        req.on('end', async () => {
            res.writeHead(200, { 'Content-Type': 'text/html' });
            var bodyObject = JSON.parse(body);
            if(bodyObject.graph == null || bodyObject.graph == ""){
                res.statusCode = 404;
                res.write("No graph given.");
                res.end();
                return;
            }
            var preresult = await materialKeys.getMaterialKeys(bodyObject.graph);
            preresult = JSON.parse(preresult);
            var searchTerm = bodyObject.terms[0];
            var lastR = 0;
            if(Object.keys(preresult).length > 1) {
                var mats = preresult["@graph"];
                for(let j of mats){
                    if(j["label"]!=null){
                        let kw = j["label"]["@value"];
                        let r = fuzz.ratio(searchTerm, kw);
                        if (r>lastR){
                            searchTerm = kw;
                            lastR = r;
                        }
                    }
                }

            }
            var result = await materials.getMaterials(searchTerm, bodyObject.graph);
            console.log(result);
            if(Object.keys(result).length > 1) {
                res.write(result);
            }else{
                res.statusCode = 404;
                res.write("No materials found.");
            }
            res.end();
        });
    }
    else if (req.url == "/materials/keywords" && req.method === 'POST') {
        let body = '';
        req.on('data', chunk => {
            body += chunk.toString(); // convert Buffer to string
        });
        req.on('end', async () => {
            res.writeHead(200, { 'Content-Type': 'text/html' });
            var bodyObject = JSON.parse(body);
            if(bodyObject.graph == null || bodyObject.graph == ""){
                res.statusCode = 404;
                res.write("No graph given.");
                res.end();
                return;
            }
            var result = await materialKeys.getMaterialKeys(bodyObject.graph);
            if(Object.keys(result).length > 1) {
                res.write(result);
            }else{
                res.statusCode = 404;
                res.write("No material keywords found.");
            }
            res.end();
        });
    }
    else if (req.url == "/compare" && req.method === 'POST') {
        let body = '';
        req.on('data', chunk => {
            body += chunk.toString(); // convert Buffer to string
        });
        req.on('end', async () => {
            res.writeHead(200, { 'Content-Type': 'text/html' });
            var bodyObject = JSON.parse(body);
            let inputConcepts = bodyObject.terms;
            let centralConcepts = await compareTerms.getCentralConcepts(graph)
            centralConcepts = centralConcepts.split('\r\n').filter((el) => el !== '' && el !== 'label')
            if(centralConcepts.length < 1) {
                res.statusCode = 404;
                res.write("no concepts to compare with");
            }else{
                var terms = {};
                for(let term of inputConcepts){
                    terms[term] = {correctTerm:"",ratio:0};
                }
                for(let concept of centralConcepts){
                    for(let term of inputConcepts){
                        let r = fuzz.ratio(concept, term);
                        if(terms[term]['ratio']<r)
                            terms[term] = {correctTerm: concept,ratio:r};
                    }
                }
                inputConcepts = new Set(); 
                for(const [key, value] of Object.entries(terms)){
                    if(value['ratio']>=acceptance){
                        inputConcepts.add(value['correctTerm']);
                    }else{
                        inputConcepts.add(key);
                    }
                }
                inputConcepts = Array.from(inputConcepts)
                const difference = _.difference(centralConcepts, inputConcepts)
                const difference2 = _.difference(inputConcepts, centralConcepts)
                const intersection = _.intersection(centralConcepts, inputConcepts)
                var mCount =  intersection.length
                if (intersection.length>1){
                    const intersectionString = intersection.join(", ")
                    mCount = mCount+ " ("+intersectionString+")" 
                } else if (intersection.length>0){
                    mCount = mCount+ " ("+intersection+")" 
                }
                let result = {
                    intersection: intersection,
                    difference: difference,
                    missing: difference2,
                    matchCount: mCount 
                }
                res.write(JSON.stringify(result));
            }
            res.end();
        });
    }
    else
        res.end('Invalid Request!');

});

server.listen(3000); 

console.log('Node.js web server at port 3000 is running..')
