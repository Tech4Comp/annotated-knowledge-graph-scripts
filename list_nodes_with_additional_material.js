'use strict';

const { post } = require('request-promise-native')
const jsonld = require('jsonld')
const _ = require('lodash');

const sparqlEndpoint = process.env.sparqlEndpoint;
//const graph = 'http://triplestore.tech4comp.dbis.rwth-aachen.de/Wissenslandkarten/data/Session_Laura'

var getNodesWithAdditionalMaterial = async function (graph) {
  const query = `PREFIX ulo: <http://uni-leipzig.de/tech4comp/ontology/>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    CONSTRUCT { ?s rdfs:label ?o . ?s ulo:continuativeMaterial ?o2 . } WHERE {
      GRAPH <${graph}> {
        ?s ulo:continuativeMaterial ?o2 ;
           rdfs:label ?o .
      }
    }`
  return get(query)
}

async function get (query, format = 'application/ld+json') {
  try {
    let response = await post({
      uri: sparqlEndpoint,
      method: 'POST',
      headers: { 'Content-Type': 'application/sparql-query', 'Accept': format },
      body: query,
      json: false
    })

    return response
  } catch (e) {
    if (e.statusCode === 404)
      return ''
    console.log(e)
    return ''
  }
}
/*
(async function () {
  try {
    let result = await getNodesWithAdditionalMaterial()
    if(!_.isEmpty(result)) {
      result = JSON.parse(result)
      if(Object.keys(result).length > 1) {
      console.log(JSON.stringify(result))
    } else abort('There is not data')
    } else abort('There is not data')
  } catch (e) {
    abort(e)
  }

  function abort(message) {
    console.error(message)
    process.exit(1)
  }
})()
*/

module.exports = {
  getMaterialKeys: async function (graph) {
    return await getNodesWithAdditionalMaterial(graph);
  }
};
