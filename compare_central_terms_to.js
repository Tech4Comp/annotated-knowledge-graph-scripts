'use strict';

const { post } = require('request-promise-native')
const jsonld = require('jsonld')
const _ = require('lodash');

const sparqlEndpoint = 'https://triplestore.tech4comp.dbis.rwth-aachen.de/Wissenslandkarten'
const graph = 'http://triplestore.tech4comp.dbis.rwth-aachen.de/Wissenslandkarten/data/Session_Laura'

async function getCentralConcepts (graph) {
  const query = `PREFIX ulo: <http://uni-leipzig.de/tech4comp/ontology/>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    SELECT DISTINCT ?label WHERE {
      GRAPH <${graph}> {
        ?s a ulo:Text ;
        ulo:centralTerm ?o .

        ?o rdfs:label ?label .
      }
    }`
  return get(query, true)
}

async function get (query, select = false, format = 'application/n-triples') {
  format = (select) ? 'text/csv' : format
  try {
    let response = await post({
      uri: sparqlEndpoint,
      method: 'POST',
      headers: { 'Content-Type': 'application/sparql-query', 'Accept': format },
      body: query,
      json: false
    })

    return response
  } catch (e) {
    if (e.statusCode === 404)
      return ''
    console.error(e)
    return ''
  }
}
/*
(async function () {
  const inputConcepts = process.argv.slice(2);
  if(inputConcepts.length < 1) {
    console.error('too few arguments')
    process.exit(1)
  }
  let centralConcepts = await getCentralConcepts(graph)
  centralConcepts = centralConcepts.split('\r\n').filter((el) => el !== '' && el !== 'label')
  if(centralConcepts.length < 1) {
    console.error('no concepts to compare with')
    process.exit(1)
  }
  const difference = _.difference(centralConcepts, inputConcepts)
  const difference2 = _.difference(inputConcepts, centralConcepts)
  const intersection = _.intersection(centralConcepts, inputConcepts)
  let result = {
    intersection: intersection,
    difference: difference,
    missing: difference2,
    matchCount: intersection.length
  }
  console.log(JSON.stringify(result))
})()

*/
module.exports = {
  getCentralConcepts: async function (term) {
    return await getCentralConcepts(term);
  }
};