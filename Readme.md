# Scripts to filter Domain Specific Knowledge Graphs (DSKGs)

This repository provides NodeJS based scripts to query specific DSKGs via Sparql. Install dependencies via `npm install` first and use `node ...` to execute these scripts.
