'use strict';

const { post } = require('request-promise-native')
const jsonld = require('jsonld')
const _ = require('lodash');

const sparqlEndpoint = process.env.sparqlEndpoint;
//const graph = 'http://tech4comp.de/knowledgeMap/ul/biwi/laura'

var getInfosAboutTerm = async function(label, graph) {
  const query = `PREFIX ulo: <http://uni-leipzig.de/tech4comp/ontology/>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    CONSTRUCT { ?s2 ?p2 ?o2 . } WHERE {
      GRAPH <${graph}> {
        ?s rdfs:label "${label}"@de ;
        ulo:continuativeMaterial ?s2 .
        optional {
          ?s2 ?p2 ?o2 .
        }
      }
    }`
  return await get(query)
}

async function get (query, format = 'application/ld+json') {
  try {
    let response = await post({
      uri: sparqlEndpoint,
      method: 'POST',
      headers: { 'Content-Type': 'application/sparql-query', 'Accept': format },
      body: query,
      json: false
    })
    return response
  } catch (e) {
    if (e.statusCode === 404)
      return ''
    console.log(e)
    return ''
  }
}
/*
(async function () {
  try {
    const terms = process.argv.slice(2);
    if(terms.length !== 1) {
      console.error('too few or too much arguments')
      process.exit(1)
    }
    let result = await getInfosAboutTerm(terms[0])
    if(!_.isEmpty(result)) {
      result = JSON.parse(result)
      if(Object.keys(result).length > 1) {
      // there is information
      console.log(JSON.stringify(result))
    } else abort('There is not data')
    } else abort('There is not data')
  } catch (e) {
    abort(e)
  }

  function abort(message) {
    console.error(message)
    process.exit(1)
  }
})()
*/
module.exports = {
  getMaterials: async function (term, graph) {
    return await getInfosAboutTerm(term, graph);
  }
};
