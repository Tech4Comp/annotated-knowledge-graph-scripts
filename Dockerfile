FROM node:12
# Create app directory
WORKDIR /usr/src/app
# copy app
Add . /usr/src/app
RUN npm install
EXPOSE 3000
CMD [ "node", "app.js" ]